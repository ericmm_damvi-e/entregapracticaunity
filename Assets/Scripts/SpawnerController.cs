using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public balaController disparo;
    public Camera cam;
    public GameObject pj;
    public Enemic[] enemics;
    public UIManager uiManager;
    int spdSpawn1 = 3;
    int spdSpawn2 = 2;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnearNaves());
        StartCoroutine(spawnearMisiles());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator spawnearNaves()
    {
        while (true)
        {
            NaveController a = (NaveController)Instantiate(enemics[0]);
            a.disparo = this.disparo;
            a.pj = this.pj;
            a.transform.position = new Vector3(cam.transform.position.x + 10, 4, 0);
            yield return new WaitForSeconds(spdSpawn1);
        }
    }
    IEnumerator spawnearMisiles()
    {
        while (true)
        {
            misilController a = (misilController)Instantiate(enemics[1]);
            a.transform.position = new Vector3(8.433764f, Random.Range(-3.64f, -0.51f), 0);
            yield return new WaitForSeconds(spdSpawn2);
        }
    }
    IEnumerator dificultad()
    {
        while (true)
        {
            if (uiManager.puntos == 500)
            {
                spdSpawn1++;
                spdSpawn2++;
            }
            else if(uiManager.puntos == 1000)
            {
                spdSpawn1++;
                spdSpawn2++;
            }
            yield return new WaitForSeconds(1);
        }
    }
}
