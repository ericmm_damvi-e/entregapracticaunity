using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor.Tilemaps;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    static int MaxJumps = 2;
    static int jumps = MaxJumps;
    public TextMeshProUGUI textMesh;
    public GameObject disparo;
    public ParticleSystemForceField ps;
    int vida = 3;
    bool cooldown = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        movement();
        StartCoroutine(disparar());
    }
    void movement()
    {
        // distance = obj1 - obj2; distance.Normalize(); this.velocity = distance*spd;
        if (this.transform.position.x <= 7.09f && this.transform.position.x >= -7.22f)
        {
            if (Input.GetKey(KeyCode.D))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(5, this.GetComponent<Rigidbody2D>().velocity.y, 0);
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(-5, this.GetComponent<Rigidbody2D>().velocity.y, 0);
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y, 0);

            }
        }
        else {
            if (this.transform.position.x > -7.22f)
            {
                this.transform.position = new Vector3(7.09f, this.transform.position.y, 0);
            }
            else
            {
                this.transform.position = new Vector3(-7.22f, this.transform.position.y, 0);
            }
        }
        if (Input.GetKeyDown(KeyCode.Space) && jumps>0)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 250));
            jumps--;
        }
    }
    public void danyar()
    {
        vida--;
        if(this.vida==0)
        {            
            SceneManager.LoadScene("GameOver");
            Destroy(this.gameObject);
        }
    }
    public int Sec = 20;
    IEnumerator tempsBala()
    {
        for(int i = 0; i < 20; i++)
        {
            Sec--;
            textMesh.text = "Cooldown: "+Sec+"s";
            yield return new WaitForSeconds(1);
        }
        Sec = 20;
        textMesh.text = "Cooldown: " + Sec + "s";
    }
    IEnumerator disparar()
    {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (!cooldown)
            {
                bool flipped = this.GetComponent<SpriteRenderer>().flipX;
                cooldown = true;
                StartCoroutine(tempsBala());
                GameObject a = Instantiate(disparo);
                if (flipped == false)
                {
                    a.transform.position = new Vector3(this.transform.position.x + 2, this.transform.position.y, this.transform.position.z);
                }
                else
                {
                    a.transform.position = new Vector3(this.transform.position.x - 2, this.transform.position.y, this.transform.position.z);

                }
                a.GetComponent<SpriteRenderer>().color = new Color(a.GetComponent<SpriteRenderer>().color.r, a.GetComponent<SpriteRenderer>().color.g, a.GetComponent<SpriteRenderer>().color.b, 0.3f);
                ParticleSystem b = Instantiate(ps.GetComponentInChildren<ParticleSystem>());
                if (flipped == false)
                {
                    b.transform.position = new Vector3(this.transform.position.x + 2, this.transform.position.y, this.transform.position.z);
                }
                else
                {
                    b.transform.position = new Vector3(this.transform.position.x - 2, this.transform.position.y, this.transform.position.z);

                }
                b.Play();
                yield return new WaitForSeconds(2);
                a.GetComponent<SpriteRenderer>().color = new Color(a.GetComponent<SpriteRenderer>().color.r, a.GetComponent<SpriteRenderer>().color.g, a.GetComponent<SpriteRenderer>().color.b, 1.0f);
                yield return new WaitForSeconds(2);
                if (flipped == false)
                {
                    a.GetComponent<Rigidbody2D>().velocity = new Vector3(9, 0, 0);
                }
                else
                {
                    a.GetComponent<Rigidbody2D>().velocity = new Vector3(-9, 0, 0);

                }
                yield return new WaitForSeconds(20);
                cooldown = false;
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.name == "Suelo")
        {
            jumps = MaxJumps;
        }
    }
}
