using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NaveController : Enemic
{
   public balaController disparo;
    public GameObject pj;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(disparar());
    }

    // Update is called once per frame
    void Update()
    {
        movement();
    }
    void movement()
    {
        this.GetComponent<Rigidbody2D>().velocity = Vector3.left*4;
    }
    IEnumerator disparar()
    {
        while (true)
        {
            balaController b = Instantiate(disparo);
            b.transform.position = this.transform.position;
            b.pj = this.pj;
            yield return new WaitForSeconds(1.5f);
        }
    }
}
