using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public int puntos = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(puntuacion());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator puntuacion()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            this.GetComponent<TextMeshProUGUI>().text = "Puntuacion: " + ++puntos;
        }
    }
}
