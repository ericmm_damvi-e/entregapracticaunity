using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxManager : MonoBehaviour
{
    public GameObject[] arr;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        arr[0].GetComponent<Rigidbody2D>().velocity = new Vector3(-2, 0, 0); 
        arr[1].GetComponent<Rigidbody2D>().velocity = new Vector3(-4, 0, 0); 
        arr[2].GetComponent<Rigidbody2D>().velocity = new Vector3(-7, 0, 0);
        if (arr[0].transform.position.x <= -9.26f)
        {
            arr[0].transform.position = new Vector3(9.35f, this.transform.position.y, this.transform.position.z);
        }else if (arr[1].transform.position.x <= -9.26f)
        {
            arr[1].transform.position = new Vector3(9.35f, this.transform.position.y, this.transform.position.z);
        }else if (arr[2].transform.position.x <= -9.26f)
        {
            arr[2].transform.position = new Vector3(9.35f, this.transform.position.y, this.transform.position.z);
        }
    }
}
