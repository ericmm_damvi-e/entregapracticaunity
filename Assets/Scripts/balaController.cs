using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class balaController : MonoBehaviour
{
    public GameObject pj;
    // Start is called before the first frame update
    void Start()
    {
        
        Vector3 distance = pj.transform.position-this.transform.position;
        distance.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = distance*6;
        //float si = Mathf.Atan2(-distance.y,-distance.x)*Mathf.Rad2Deg;
        //transform.Rotate(0,0,si);
        this.transform.right = distance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CharacterController c = collision.GetComponent<CharacterController>();
            c.danyar();
        }
        Destroy(this.gameObject);
    }
}
